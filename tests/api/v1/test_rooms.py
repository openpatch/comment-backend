import uuid
from openpatch_comment.models.room import Room
from tests.base_test import BaseTest
from tests.mock import ids


class TestRooms(BaseTest):
    def test_post_room_user(self):
        room = {"id": str(uuid.uuid4()), "type": "test"}
        response = self.client.post(
            f"/v1/rooms", headers=self.fake_headers["user"], json=room
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("room_id", response.json)
        self.assertEqual(response.json["room_id"], room["id"])

        room = {"id": str(uuid.uuid4())}
        response = self.client.post(
            f"/v1/rooms", headers=self.fake_headers["user"], json=room
        )
        self.assertEqual(response.status_code, 400)

    def test_get_rooms_user(self):
        response = self.client.get(
            f"/v1/rooms/{ids[0]}", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("room", response.json)
        self.assertIn("comments", response.json["room"])
        comments = response.json["room"]["comments"]
        self.assertEqual(len(comments), 1)
        self.assertIn("username", comments[0]["member"])

        response = self.client.get(
            f"/v1/rooms/{uuid.uuid4()}", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 404)

    def test_delete_room_user(self):
        count = Room.query.count()
        response = self.client.delete(
            f"/v1/rooms/{ids[1]}", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 200)
        count_after = Room.query.count()
        self.assertNotEqual(count, count_after)

    def test_post_room_user(self):
        comment_json = {"message": "Hi what is going on?"}
        response = self.client.post(
            f"/v1/rooms/{ids[1]}", headers=self.fake_headers["user"], json=comment_json
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            f"/v1/rooms/{ids[1]}", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("room", response.json)
        self.assertIn("comments", response.json["room"])
        comments = response.json["room"]["comments"]
        self.assertEqual(len(comments), 1)
        self.assertIn("username", comments[0]["member"])
