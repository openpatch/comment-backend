from openpatch_comment.models.room import Room
from openpatch_comment.models.member import Member
from openpatch_comment.models.comment import Comment
from openpatch_core.database import db
import uuid

ids = [str(uuid.uuid4()) for i in range(10)]

members = [{"id": str(uuid.uuid4()), "username": "test_user"}]

comments = [{"room_id": ids[0], "member_id": members[0]["id"], "message": "Test"}]


def mock():
    for id in ids:
        db.session.add(Room(id=id, type="item"))
    db.session.commit()

    for member in members:
        db.session.add(Member(**member))

    for comment in comments:
        db.session.add(Comment(**comment))

    db.session.commit()

