from openpatch_comment.models.member import Member
from openpatch_core.schemas import ma


class MemberSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Member

        only = ["id", "username", "avatar_id", "full_name"]


MEMBER_SCHEMA = MemberSchema()
MEMBERS_SCHEMA = MemberSchema(many=True)
