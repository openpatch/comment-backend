from openpatch_comment.models.comment import Comment
from openpatch_core.schemas import ma
from openpatch_comment.api.v1.schemas.member import MemberSchema


class CommentSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Comment
        load_instance = True
        include_relationships = True

    member = ma.Nested(MemberSchema, only=["id", "username", "full_name", "avatar_id"])


COMMENT_SCHEMA = CommentSchema()
COMMENTS_SCHEMA = CommentSchema(many=True)
