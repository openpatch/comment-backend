from marshmallow import fields, post_dump
from openpatch_comment.api.v1.schemas.comment import CommentSchema
from openpatch_comment.models.room import Room
from openpatch_core.schemas import ma


class RoomSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Room
        load_instance = True
        include_relationships = True

    comments = fields.Nested(CommentSchema, many=True)


ROOM_SCHEMA = RoomSchema()
ROOMS_SCHEMA = RoomSchema(many=True)
