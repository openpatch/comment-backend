from flask import jsonify
from openpatch_comment.api.v1 import api

BASE_URL = '/'


@api.route(BASE_URL + '/healthcheck', methods=['GET'])
def get_healthcheck():
    """
    @api {get} /healthcheck Request Health information
    @apiVersion 1.0.0
    @apiName Healthcheck
    @apiGroup Root

    @apiSuccess {String} status healthy.
    """
    return jsonify({'status': 'healthy'}), 200

