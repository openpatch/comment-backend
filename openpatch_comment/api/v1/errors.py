from openpatch_core.errors import error_manager as em


def resource_not_found():
    return em.make_json_error(404, message='Resource not found', code=110)


def invalid_json(errors):
    return em.make_json_error(
        400, message="Invalid JSON in request body", details=errors, code=1)


def no_json():
    # 201
    return em.make_json_error(
        400, message="JSON in request body is missing", code=201)


def no_message():
    return em.make_json_error(
        400, message="Comment message can not be empty", code=202
    )
