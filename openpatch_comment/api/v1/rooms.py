from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_comment.api.v1 import api, errors
from openpatch_comment.api.v1.schemas.comment import COMMENT_SCHEMA
from openpatch_comment.api.v1.schemas.room import ROOM_SCHEMA
from openpatch_comment.api.v1.schemas.member import MEMBER_SCHEMA
from openpatch_comment.models.comment import Comment
from openpatch_comment.models.member import Member
from openpatch_comment.models.room import Room
from openpatch_core.database import db
from openpatch_core.jwt import get_jwt_claims, jwt_required
from openpatch_core.rabbitmq import rabbit

base_url = "/rooms"


@api.route(base_url, methods=["POST"])
def rooms():
    room_json = request.get_json()

    if not room_json:
        return errors.no_json()

    try:
        room = ROOM_SCHEMA.load(room_json, session=db.session)
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(room)
    db.session.commit()

    return jsonify({"room_id": room.id})


@api.route(base_url + "/<room_id>", methods=["GET", "DELETE", "POST"])
def room(room_id):
    if request.method == "GET":
        return get_room(room_id)
    elif request.method == "DELETE":
        return delete_room(room_id)
    elif request.method == "POST":
        return post_room(room_id)


def get_room(room_id):
    room = Room.query.get(room_id)

    if not room:
        return errors.resource_not_found()

    return jsonify({"room": ROOM_SCHEMA.dump(room)})


@jwt_required
def delete_room(room_id):

    room = Room.query.filter_by(id=room_id).first()

    db.session.delete(room)
    db.session.commit()

    return jsonify({"status": "success"})


@jwt_required
def post_room(room_id):
    jwt_claims = get_jwt_claims()
    comment_json = request.get_json()
    room = Room.query.get(room_id)

    if not room:
        return errors.resource_not_found()

    if not comment_json:
        return errors.no_json()

    if not comment_json.get("message"):
        return errors.no_message()

    member = Member.get_or_create(jwt_claims)
    comment_json["room"] = room.id
    comment_json["approved"] = True

    try:
        comment = COMMENT_SCHEMA.load(comment_json, session=db.session)
        comment.member = member
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(comment)
    db.session.commit()

    rabbit.publish_activity(
        "comment",
        {"id": room.id, "type": room.type, "data": COMMENT_SCHEMA.dump(comment)},
        MEMBER_SCHEMA.dump(member),
    )

    return jsonify({"comment_id": comment.id})


@api.route(base_url + "/<room_id>/comments/<comment_id>", methods=["PUT", "DELETE"])
@jwt_required
def room_comment(room_id, comment_id):
    if request.method == "PUT":
        return put_room_comment(room_id, comment_id)
    elif request.method == "DELETE":
        return delete_room_comment(room_id, comment_id)


def put_room_comment(room_id, comment_id):
    comment_json = request.get_json()

    comment = Comment.query.get(comment_id)

    if not comment:
        return errors.resource_not_found()

    comment.message = comment_json.get("message")
    db.session.commit()

    return jsonify({"status": "success"})


def delete_room_comment(room_id, comment_id):
    comment = Comment.query.get(comment_id)

    if not comment:
        return errors.resource_not_found()

    db.session.delete(comment)
    db.session.commit()

    return jsonify({"status": "success"})
