from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
from openpatch_core.rabbitmq import rabbit
import uuid


class Comment(Base):
    __tablename__ = gt("comment")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)

    parent_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("comment"))))

    member_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("member"))))
    member = db.relationship("Member", back_populates="comments")

    room_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("room"))))
    room = db.relationship("Room", back_populates="comments")

    # store extra data which this microservice will not handle, but some other
    # will make use of
    extra = db.Column(db.JSON)

    message = db.Column(db.Text)

    approved = db.Column(db.Boolean)
