from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
import uuid


class Room(Base):
    __tablename__ = gt("room")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    type = db.Column(db.String(256), nullable=False)

    comments = db.relationship(
        "Comment", back_populates="room", order_by="desc(Comment.created_on)"
    )
