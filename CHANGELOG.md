## [1.2.3](https://gitlab.com/openpatch/comment-backend/compare/v1.2.2...v1.2.3) (2020-06-07)


### Bug Fixes

* mocking of data ([6f8a88a](https://gitlab.com/openpatch/comment-backend/commit/6f8a88a569fca062aaec2c4ace1d2e8418b92735))
* use mock rabbitmq ([d9544a8](https://gitlab.com/openpatch/comment-backend/commit/d9544a8c37b83bf840c6eceb909c654b715914b1))
