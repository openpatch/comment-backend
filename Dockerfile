FROM registry.gitlab.com/openpatch/flask-microservice-base:v3.7.0

COPY "." "/var/www/app"

ENV OPENPATCH_SERVICE_NAME=comment
ENV OPENPATCH_AUTHENTIFICATION_SERVICE=https://api.openpatch.app/authentification
ENV OPENPATCH_DB=sqlite+pysqlite:///database.db
ENV OPENPATCH_ORIGINS=*

RUN pip3 install -r /var/www/app/requirements.txt
